//
//  Application.h
//  ErrorCorrectionExperiment
//
//  Created by Andy Frey on 4/13/15.
//  Copyright (c) 2015 Andy Frey. All rights reserved.
//

#ifndef __ErrorCorrectionExperiment__Application__
#define __ErrorCorrectionExperiment__Application__

#include <stdio.h>
#include "Packet.h"


class Application: public PacketDelegate {
    // instance variables
    unsigned long _loopTime;
    
public:
    Application();
    void main();
    
    // packet delegate members
    void didReceivePacket(Packet *p);
    void didReceiveBadPacket(uint8_t err);

};

#endif /* defined(__ErrorCorrectionExperiment__Application__) */
