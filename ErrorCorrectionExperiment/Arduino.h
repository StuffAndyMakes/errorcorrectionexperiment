//
//  Arduino.h
//  ErrorCorrectionExperiment
//
//  Created by Andy Frey on 4/29/15.
//  Copyright (c) 2015 Andy Frey. All rights reserved.
//

#ifndef ErrorCorrectionExperiment_Arduino_h
#define ErrorCorrectionExperiment_Arduino_h

typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned int uint32_t;


class HardwareSerial {
    bool _open;
    uint16_t _speed;
    bool _available;
    uint8_t _buffer;
    
public:

    HardwareSerial();
    void begin(uint16_t s);
    void close();
    bool available();
    void write(uint8_t c);
    int read();

    void introduceErrors();
    void dumpBuffer();

    void loop();

};

#endif
