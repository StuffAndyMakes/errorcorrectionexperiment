//
//  Application.cpp
//  ErrorCorrectionExperiment
//
//  Created by Andy Frey on 4/13/15.
//  Copyright (c) 2015 Andy Frey. All rights reserved.
//

#include "Application.h"
#include <iostream>
#include <string.h>
#include <stdio.h>
#include "Arduino.h"
#include "Packet.h"

using namespace std;


Application::Application() {
    _loopTime = 0;
}

void Application::didReceivePacket(Packet *p) {
    printf("didReceivePacket() fired!\n");
    uint8_t data[255];
    uint8_t len = p->copyBytes(data, 255);
    printf("Data length: %d\n", len);
    if (p->getDataType() == Packet::TYPE_BYTES) {
        for (uint8_t i = 0; i < len; i++) {
            printf("  %hhu: 0x%x (%d)\n", i, data[i], data[i]);
        }
    } else {
        char s[250];
        p->copyString(s, 250);
        printf("  |%s|\n", s);
    }
}

void Application::didReceiveBadPacket(uint8_t err) {
    printf("Error %d receiving packet: ", err);
    switch (err) {
        case Packet::ERROR_CRC:
            printf("CRC Mismatch\n");
            break;
        case Packet::ERROR_FRAME:
            printf("Framing (length or missing end)\n");
            break;
        case Packet::ERROR_LENGTH:
            printf("Data Length\n");
            break;
        case Packet::ERROR_OVERFLOW:
            printf("Buffer Overflow\n");
            break;
            
        default:
            break;
    }
}

void Application::main() {

    HardwareSerial Serial;
    Serial.begin(19200);

    Packet p;
    p.setDelegate(this);

    uint8_t bytes[] = {1, 2, 3, Packet::ESCAPE, Packet::FRAME_END, 6, 7, 8, 9};
    printf("Packet length: %u\n", p.packetizeBytes(bytes, 9));
    p.introduceErrors();
    p.send(&Serial);
    Serial.dumpBuffer();
    Serial.introduceErrors();
    Serial.dumpBuffer();
    p.receive(&Serial);

    char s[] = "This is a test of a long string. This makes it even longer! And this adds more to the string.";
    printf("Packet length: %u\n", p.packetizeString(s));
    p.send(&Serial);
    Serial.dumpBuffer();
    Serial.introduceErrors();
    Serial.dumpBuffer();
    p.receive(&Serial);

    system("stty raw"); // makes getchar() return char without need for pressing enter

    for (int k = getchar(); ; k = getchar()) {
        switch (k) {

            case 's':
            case 'S':
                printf("Send!\n");
                break;

            case 'q':
            case 'Q':
                system("stty cooked"); // set console tty back before quitting
                return;
                break;

            default:
                printf("Invalid command.\n");
                break;
        }
    }
    
}
