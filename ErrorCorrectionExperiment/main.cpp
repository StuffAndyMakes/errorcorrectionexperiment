//
//  main.cpp
//  ErrorCorrectionExperiment
//
//  Created by Andy Frey on 4/5/15.
//  Copyright (c) 2015 Andy Frey. All rights reserved.
//

#include <stdio.h>
#include "Application.h"

int main(int argc, const char * argv[]) {

    Application app;
    app.main();

    return 0;
}
