//
//  Arduino.cpp
//  ErrorCorrectionExperiment
//
//  Created by Andy Frey on 4/29/15.
//  Copyright (c) 2015 Andy Frey. All rights reserved.
//

#include <stdio.h>
#include "Arduino.h"



HardwareSerial::HardwareSerial() {
    _open = false;
    _speed = 0;
    _buffer = 0;
    _available = false;
}

void HardwareSerial::begin(uint16_t s) {
    _buffer = 0;
    _available = false;
    _speed = s;
    _open = true;
}

void HardwareSerial::close() {
    _open = false;
    _speed = 0;
    _buffer = 0;
    _available = false;
}

void HardwareSerial::write(uint8_t c) {
    if (_open == false) return;
    _buffer = c;
    _available = true;
}

bool HardwareSerial::available() {
    return _available;
}

int HardwareSerial::read() {
    if (_open == false) return -1;
    return _buffer;
}

void HardwareSerial::introduceErrors() {
    printf("(adding serial errors)\n");
    _buffer = 186;
}

void HardwareSerial::dumpBuffer() {
    printf("Serial buffer: %02x\n", _buffer);
}

void HardwareSerial::loop() {
    
}
