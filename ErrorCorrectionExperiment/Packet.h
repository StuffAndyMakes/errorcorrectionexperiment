//
//  Packet.h
//  ErrorCorrectionExperiment
//
//  Created by Andy Frey on 4/5/15.
//  Copyright (c) 2015 Andy Frey. All rights reserved.
//

#ifndef __ErrorCorrectionExperiment__Packet__
#define __ErrorCorrectionExperiment__Packet__

#include <stdio.h>
#include "Arduino.h"


#define CRC16 ((uint16_t)(0x8005))

// 256 - (1B start) - (1B len) - (1B type) - (2B CRC16) - (1B stop) = 250
#define MAX_DATA_SIZE (250)

#define STATE_NONE 0
#define STATE_WAIT_START 1
#define STATE_LENGTH 2
#define STATE_CRC1 3
#define STATE_CRC2 4
#define STATE_TYPE 5
#define STATE_DATA 6
#define STATE_ESCAPE 7
#define STATE_END_WAIT 8
#define STATE_END_FRAME 9
#define STATE_SENDING 10

#define DATA_UNKNOWN 0
#define DATA_BYTES 1
#define DATA_CHARS 2


class Packet;

class PacketDelegate {
    
public:
    virtual void didReceivePacket(Packet *p) = 0;
    virtual void didReceiveBadPacket(uint8_t err) = 0;

};


class Packet {

    uint8_t _state = STATE_NONE;
    uint8_t _data[MAX_DATA_SIZE];
    uint8_t _dataType;
    uint8_t _dataLength;
    uint8_t _dataPos;
    uint16_t _crc;
    PacketDelegate *_delegate;
    
    uint16_t _crc16(const uint8_t *data, uint16_t size);
    
    void callDelegateError(uint8_t err);
    
public:

    static const uint8_t TYPE_BYTES = DATA_BYTES;
    static const uint8_t TYPE_CHARS = DATA_CHARS;

    static const uint8_t ERROR_CRC = 1;
    static const uint8_t ERROR_FRAME = 2;
    static const uint8_t ERROR_LENGTH = 3;
    static const uint8_t ERROR_OVERFLOW = 4;

    static const uint8_t FRAME_START = (uint8_t)0b10101010;
    static const uint8_t FRAME_END = (uint8_t)0b01010101;
    static const uint8_t ESCAPE = (uint8_t)0x5c; // '\'

    Packet();

    void setDelegate(PacketDelegate *d);

    uint8_t packetizeBytes(uint8_t *p, uint8_t l);
    uint8_t packetizeString(char *s);

    uint8_t getDataLength();
    uint8_t getDataType();

    uint8_t copyBytes(uint8_t *b, uint8_t bufsize);
    uint8_t copyString(char *s, uint8_t maxlen);

    bool matchesCRC(Packet *p);
    
    uint8_t send(HardwareSerial *s);
    void receive(HardwareSerial *s);

    void introduceErrors();

};

#endif /* defined(__ErrorCorrectionExperiment__Packet__) */
