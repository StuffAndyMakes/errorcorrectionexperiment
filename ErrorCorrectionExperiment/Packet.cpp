//
//  Packet.cpp
//  ErrorCorrectionExperiment
//
//  Created by Andy Frey on 4/5/15.
//  Copyright (c) 2015 Andy Frey. All rights reserved.
//

#include <stdio.h>
#include "Packet.h"


#define MIN(x,y) (x < y ? x : y)
#define MAX(x,y) (x > y ? x : y)


void toBin(uint8_t c, char *s) {
    s[8] = '\0';
    uint8_t bv = 128;
    for (uint8_t b = 0; b < 8; b++) {
        s[b] = (c & bv) == bv ? '1' : '0';
        bv >>= 1;
    }
}


Packet::Packet() {
    _state = STATE_NONE;
    _crc = 0;
    for (uint8_t b = 0; b < MAX_DATA_SIZE; b++) {
        _data[b] = 0;
    }
    _dataLength = 0;
    _dataType = DATA_UNKNOWN;
    _dataPos = 0;
}

void Packet::setDelegate(PacketDelegate *d) {
    _delegate = d;
}

uint16_t Packet::_crc16(const uint8_t *data, uint16_t size) {
    uint16_t out = 0;
    int bits_read = 0, bit_flag;
    // Sanity check
    if(data == NULL) return 0;
    while(size > 0) {
        bit_flag = out >> 15;
        // Get next bit:
        out <<= 1;
        // item a) work from the least significant bits
        out |= (*data >> bits_read) & 1;
        // Increment bit counter:
        bits_read++;
        if(bits_read > 7) {
            bits_read = 0;
            data++;
            size--;
        }
        // Cycle check:
        if(bit_flag) out ^= CRC16;
    }
    // item b) "push out" the last 16 bits
    int i;
    for (i = 0; i < 16; ++i) {
        bit_flag = out >> 15;
        out <<= 1;
        if(bit_flag) out ^= CRC16;
    }
    // item c) reverse the bits
    uint16_t crc = 0;
    i = 0x8000;
    int j = 0x0001;
    for (; i != 0; i >>=1, j <<= 1) {
        if (i & out) crc |= j;
    }
    return crc;
}

uint8_t Packet::packetizeBytes(uint8_t *p, uint8_t l) {
    if (l == 0) return 0;
    if (l > MAX_DATA_SIZE) {
        l = MAX_DATA_SIZE;
    }
    _dataLength = 0;
    for (uint8_t c = 0; c < l; c++) {
        _data[_dataLength++] = p[c];
    }
    _dataType = DATA_BYTES;
    _crc = _crc16(_data, _dataLength);
    return _dataLength;
}

uint8_t Packet::packetizeString(char *s) {
    if (s[0] == '\0') return 0;
    _dataLength = 0;
    for (uint8_t c = 0; (s[c] != '\0') && (c < MAX_DATA_SIZE) && (_dataLength < MAX_DATA_SIZE); c++) {
        _data[_dataLength++] = (uint8_t)s[c];
        _dataType = DATA_CHARS;
    }
    _crc = _crc16(_data, _dataLength);
    return _dataLength;
}

uint8_t Packet::getDataLength() {
    return _dataLength;
}

uint8_t Packet::getDataType() {
    return _dataType;
}

uint8_t Packet::copyBytes(uint8_t *buf, uint8_t bufsize) {
    uint8_t b = 0;
    for (b = 0; b < MIN(getDataLength(), bufsize); b++) {
        buf[b] = _data[b];
    }
    return b;
}

uint8_t Packet::copyString(char *str, uint8_t maxlen) {
    uint8_t b;
    for (b = 0; b < MIN(getDataLength(), maxlen); b++) {
        str[b] = _data[b];
    }
    str[b] = '\0';
    return b;
}

bool Packet::matchesCRC(Packet *p) {
    return (_crc == p->_crc);
}

/*
 *  Blocks until data is sent
 */
uint8_t Packet::send(HardwareSerial *s) {
    if (_dataLength == 0 || s == NULL) {
        return 0;
    }
    printf("Packet::send() sending packet:\n");
    uint8_t bytesSent = 0;
    _state = STATE_SENDING;
    printf("  FRAME_START  %02x   (1 byte)\n", FRAME_START);
    s->write(FRAME_START); bytesSent++;
    printf("  Data Length  %02x   (1 byte)\n", _dataLength);
    s->write(_dataLength); bytesSent++;
    printf("  CRC          %04x (2 bytes)\n", _crc);
    s->write(_crc & 0xff); bytesSent++; // LSB first
    s->write((_crc >> 8) & 0xff); bytesSent++; // MSB last
    printf("  Data Type    %02x   (1 byte)\n", _dataType);
    s->write(_dataType); bytesSent++;
    printf("  Data         ");
    uint8_t b = 0, dataCount = 0;
    for (b = 0; b < _dataLength; b++) {
        if ((_data[b] == ESCAPE) || (_data[b] == FRAME_START) || (_data[b] == FRAME_END)) {
            printf("\\");
            s->write(ESCAPE); bytesSent++; dataCount++;
        }
        if (_dataType == DATA_BYTES) {
            printf("%02x ", _data[b]);
        } else {
            printf("%c", _data[b]);
        }
        s->write(_data[b]); bytesSent++; dataCount++;
    }
    printf("  (%d bytes)\n", dataCount);
    printf("  FRAME_END    %02x   (1 byte)\n", FRAME_END);
    s->write(FRAME_END); bytesSent++;
    printf("  Message length: %d\n", bytesSent);
    _state = STATE_NONE;
    return bytesSent;
}

void Packet::callDelegateError(uint8_t err) {
    _delegate->didReceiveBadPacket(err);
}

void Packet::receive(HardwareSerial *s) {

    _state = STATE_WAIT_START;

    uint8_t c = '\0';
    while (s->available()) {
        c = s->read();

        // first, check for buffer overflow
        if (_dataPos >= MAX_DATA_SIZE) {
            callDelegateError(ERROR_OVERFLOW);
        }

        switch (_state) {

            case STATE_WAIT_START:
                if (c == FRAME_START) {
                    _state = STATE_LENGTH;
                }
                break;

            case STATE_LENGTH:
                _dataLength = c;
                _state = STATE_CRC1;
                break;

            case STATE_CRC1:
                _crc = c;
                _state = STATE_CRC2;
                break;
                
            case STATE_CRC2:
                _crc |= (c << 8);
                _state = STATE_TYPE;
                break;

            case STATE_TYPE:
                _dataType = c;
                _dataPos = 0;
                _state = STATE_DATA;
                break;
                
            case STATE_DATA:
                if (c == ESCAPE) {
                    _state = STATE_ESCAPE;
                } else if (_dataPos == _dataLength) {
                    _state = STATE_END_WAIT;
                    // TODO: Mark time to start timeout counter
                } else {
                    _data[_dataPos++] = c;
                }
                break;
                
            case STATE_ESCAPE:
                _data[_dataPos++] = c;
                _state = STATE_DATA;
                break;

            case STATE_END_WAIT:
                if (c == FRAME_END) {
                    _state = STATE_END_FRAME;
                }
                break;

            case STATE_END_FRAME:
                _state = STATE_NONE;
                if (_delegate != NULL) {
                    // talk to delegate object
                    if (_dataPos == _dataLength) {
                        // length of data is good...
                        if (_crc == _crc16(_data, _dataLength)) {
                            // ...and CRC matches OK
                            _delegate->didReceivePacket(this);
                        } else {
                            // ...but, CRC doesn't match
                            callDelegateError(ERROR_CRC);
                        }
                    } else {
                        // data length is not what was expected
                        callDelegateError(ERROR_LENGTH);
                    }
                }
                break;
                
            default:
                break;
        }

    } // end while(s->available())

}

void Packet::introduceErrors() {
//    _dataLength = 7; // change length arbitrarily
//    _data[0] = 255; // change a byte
    _dataPos--; // mess with read-in length
}
