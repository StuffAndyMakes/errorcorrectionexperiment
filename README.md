# README #

This is a very simple error-correcting packetizing class for communicating over serial between two Arduino or AVR microcontrollers. It provides the receiving end with a 16-bit CRC check and data length, as well as data length and data type (for convenience) fields and frame start/end bytes.

### What is this repository for? ###

The Office Chairiot Mark II is a 20 MPH motorized office chair that uses serial communications between the control panel or wireless remote control and the chassis (where the motors are). Without error correction, sometimes bad bits cause erratic behavior. To counter this issue, the simple commands sent from remote control to chassis are wrapped in a frame that provides rudimentary error indication for the receiving end of the transmission.

This is written (at the moment) as a test project for OS X, but I've mocked the Arduino code to make it portable, once I've finished testing it. Keep an eye on this project for updates as to where the final home is for the Packet class will be (and its related classes, like the DelegatedSerial class).

### How do I get set up? ###

To use this class (as of the writing of this README, this hasn't been tested on live equipment), just grab the Packet.cpp and Packet.h files and use them in your Arduino project. I use embedXcode+ and Apple's Xcode IDE, which is what this was really written for, but you should be able to drop it into the Arduino IDE without issue, I would think.

### Contribution guidelines ###

If you have ideas, fixes, issues, talk to me. I'd like this to remain as lightweight and simple to use as possible. Its core mission is simply to give the receiving end of a UART transmission some way to make sure the data they pull from the frame is intact. It needs to have as tiny a footprint in RAM as possible and work smoothly with the Arduino HardwareSerial class.

### Who do I talk to? ###

Andy /at/ Stuff Andy Makes /little dotty thing/ com is the email.

You can also head over to http://StuffAndyMakes.com to follow the stuff I make.

This project is for the Office Chairiot Mark II, which has its own website: http://OfficeChairiot.com